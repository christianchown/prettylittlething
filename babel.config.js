module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ts', '.js', '.json'],
        alias: {
          __mocks__: './src/__mocks__',
          __tests__: './src/__tests__',
          api: './src/api',
          components: './src/components',
          lib: './src/lib',
          screens: './src/screens',
          state: './src/state',
        },
      },
    ],
  ],
};
