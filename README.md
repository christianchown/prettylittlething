# Prettylittlething coding exercise

Christian Chown / [christianchown.com](https://www.christianchown.com) / [gitlab](https://gitlab.com/christianchown) / [github](https://github.com/christianchown) / [@christianchown](https://twitter.com/christianchown)

## Install / run

1. Clone this repo
2. `yarn`
3. `npx pod-install`
4. `yarn ios` / `yarn android`

![Coding demo](./demo.gif)

## Test

- `yarn test`

## Why xstate for state management?

Someone smarter than me said _"Your UI is a state machine. The only question is whether it is defined explictly or implicitly."_.

I've had a lot of recent success by defining it explicitly.
