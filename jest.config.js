module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
  setupFilesAfterEnv: [
    '@testing-library/jest-native/extend-expect',
    '<rootDir>/src/__tests__/jestSetup.ts',
  ],
  testPathIgnorePatterns: ['__tests__/*'],
  reporters: ['default'],
  moduleNameMapper: {
    '.+\\.(png|jpg|ttf|woff|woff2)$': 'babel-jest',
    '.+\\.svg$': '<rootDir>/src/__mocks__/svgMock.ts',
  },
};
