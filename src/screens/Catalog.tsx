import React, { useEffect } from 'react';
import { ListRenderItemInfo, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useSelector } from '@xstate/react';
import Screen from 'components/Screen';
import { useStatechart } from 'state/Statechart';
import { eventSelector, productsSelector } from 'state/selectors';
import { FlatList } from 'react-native-gesture-handler';
import { Product } from 'api/product';
import ProductListItem from 'components/ProductListItem';
import Spacer from 'components/Spacer';
import { usePreviousIfDefined } from 'lib/usePrevious';

function ProductItem({ item }: ListRenderItemInfo<Product>) {
  return <ProductListItem product={item} />;
}

function ProductSpacer() {
  return <Spacer height={30} />;
}

const styles = StyleSheet.create({
  content: { flexGrow: 1 },
  list: { height: '100%' },
});

export default function Catalog() {
  const navigation = useNavigation();
  const service = useStatechart();
  const products = useSelector(service, productsSelector);
  const event = useSelector(service, eventSelector);
  const viewCategory =
    event.type === 'VIEW_CATEGORY' ? event.category : undefined;
  const prevCategory = usePreviousIfDefined(viewCategory);
  const title = viewCategory || prevCategory || 'Product catalog';
  useEffect(() => {
    navigation.setOptions({ title });
  }, [navigation, title]);
  return (
    <Screen testID="Catalog">
      <FlatList
        style={styles.list}
        contentContainerStyle={styles.content}
        renderItem={ProductItem}
        data={products}
        ItemSeparatorComponent={ProductSpacer}
      />
    </Screen>
  );
}
