import React from 'react';
import { State } from 'xstate';
import { render, cleanup, act, fireEvent } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import { Basket } from 'state/basket';
import App from '../App';

const stateWithBasket = (basket: Basket[] = [{ id: 1, quantity: 1 }]) =>
  appMachine.resolveState(
    State.create({
      ...appMachine.initialState,
      context: {
        ...appMachine.initialState.context,
        products: [
          {
            id: 1,
            name: 'test product',
            colour: 'blue',
            price: 3.99,
            img: 'img.png',
          },
        ],
        basket,
      },
      value: { checkout: 'basket' },
    }),
  );

describe('Basket', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const state = stateWithBasket();
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, queryByText } = renderAPI;
    expect(queryByTestId('Basket')).not.toBeNull();
    expect(queryByText('test product')).not.toBeNull();
    expect(queryByText(/total: £3.99/i)).not.toBeNull();
  });

  it('goes through to the payment page', async () => {
    const state = stateWithBasket();
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, getByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('toPayment'), 'onPress'));
    expect(queryByTestId('Payment')).not.toBeNull();
  });

  it('does not go through to the payment page with an empty basket', async () => {
    const state = stateWithBasket([]);
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, getByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('toPayment'), 'onPress'));
    expect(queryByTestId('Payment')).toBeNull();
  });

  it('allows you to remove items', async () => {
    const state = stateWithBasket();
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByText, getByTestId, queryByTestId } = renderAPI;
    expect(queryByTestId('quantity')?.children[0]).toBe('1');
    await act(async () => fireEvent(getByTestId('bin1'), 'onPress'));
    expect(queryByText('test product')).toBeNull();
    expect(queryByTestId('quantity')?.children[0]).toBe('0');
    expect(queryByText(/total: £0.00/i)).not.toBeNull();
  });

  it('allows you to change the quantity', async () => {
    const state = stateWithBasket();
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { getByTestId, queryByTestId, queryByText } = renderAPI;
    expect(queryByTestId('quantity')?.children[0]).toBe('1');
    await act(async () => fireEvent(getByTestId('adjust1'), 'onPress'));
    await act(async () =>
      fireEvent(getByTestId('picker1'), 'onValueChange', 2),
    );
    expect(queryByTestId('quantity')?.children[0]).toBe('2');
    expect(queryByText(/total: £7.98/i)).not.toBeNull();
  });

  it('allows you to view the products', async () => {
    const state = stateWithBasket();
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, getByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('view1'), 'onPress'));
    expect(queryByTestId('ProductPage')).not.toBeNull();
  });
});
