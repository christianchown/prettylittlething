import React, { useCallback, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useStatechart } from 'state/Statechart';
import { useSelector } from '@xstate/react';
import { eventSelector, productsSelector } from 'state/selectors';
import { usePreviousIfDefined } from 'lib/usePrevious';
import Button from 'components/Button';
import { findProduct } from 'api/product';
import Screen from 'components/Screen';
import { Bold, ProductDetail } from 'components/Text';
import ImageWithPlaceholder from 'components/ImageWithPlaceholder';
import { formatPrice } from 'lib/formatPrice';

const styles = StyleSheet.create({
  wrap: {},
  textWrap: {
    marginTop: 20,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: {
    width: '100%',
    flex: 1,
  },
  container: { flex: 1, justifyContent: 'space-between' },
});

export default function ProductPage() {
  const navigation = useNavigation();
  const service = useStatechart();
  const { send } = service;
  const event = useSelector(service, eventSelector);
  const products = useSelector(service, productsSelector);
  const viewId = event.type === 'VIEW_PRODUCT' ? event.id : undefined;
  const prevId = usePreviousIfDefined(viewId);
  const id = viewId || prevId || 0;
  const addToBasket = useCallback(
    () => send({ type: 'ADD_TO_BASKET', id }),
    [send, id],
  );
  const product = products.find(findProduct(id));
  useEffect(() => {
    navigation.setOptions({ title: product?.name });
  }, [navigation, product?.name]);
  if (!product) return null;
  const { colour, price, img } = product;
  return (
    <Screen testID="ProductPage">
      <View style={styles.container}>
        <ImageWithPlaceholder style={styles.image} source={{ uri: img }} />
        <View style={styles.wrap}>
          <View style={styles.textWrap}>
            <ProductDetail>
              Colour: <Bold>{colour}</Bold>
            </ProductDetail>
            <ProductDetail>
              Price: <Bold>£{formatPrice(price)}</Bold>
            </ProductDetail>
          </View>
        </View>
        <Button onPress={addToBasket} testID="addToBag">
          ADD TO YOUR BAG
        </Button>
      </View>
    </Screen>
  );
}
