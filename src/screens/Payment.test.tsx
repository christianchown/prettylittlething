import React from 'react';
import { State } from 'xstate';
import { render, act, cleanup } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    value: { checkout: 'payment' },
  }),
);

describe('Payment', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId } = renderAPI;
    expect(queryByTestId('Payment')).not.toBeNull();
  });
});
