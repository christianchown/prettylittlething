import React from 'react';
import { State } from 'xstate';
import { render, cleanup, act, fireEvent } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    context: {
      ...appMachine.initialState.context,
      products: [
        {
          id: 1,
          name: 'test product',
          colour: 'blue',
          price: 3.99,
          img: 'img.png',
        },
      ],
    },
    value: { catalog: 'product' },
    _event: {
      ...appMachine.initialState._event,
      name: 'VIEW_PRODUCT',
      data: { type: 'VIEW_PRODUCT', id: 1 },
    },
  }),
);

describe('ProductPage', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, queryByText } = renderAPI;
    expect(queryByTestId('ProductPage')).not.toBeNull();
    expect(queryByText('blue')).not.toBeNull();
    expect(queryByText('£3.99')).not.toBeNull();
  });

  it('increases the quantity by one after clicking add to bag', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { getByTestId, queryByTestId } = renderAPI;
    expect(queryByTestId('quantity')?.children[0]).toBe('0');
    await act(async () => fireEvent(getByTestId('addToBag'), 'onPress'));
    expect(queryByTestId('quantity')?.children[0]).toBe('1');
    await act(async () => fireEvent(getByTestId('addToBag'), 'onPress'));
    expect(queryByTestId('quantity')?.children[0]).toBe('2');
  });

  it('does not go to basket if it is empty', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { getByTestId, queryByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('checkout'), 'onPress'));
    expect(queryByTestId('Basket')).toBeNull();
  });

  it('does goes to the basket after clicking add to bag', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { getByTestId, queryByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('addToBag'), 'onPress'));
    await act(async () => fireEvent(getByTestId('checkout'), 'onPress'));
    expect(queryByTestId('Basket')).not.toBeNull();
  });
});
