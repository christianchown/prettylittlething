import React from 'react';
import { ListRenderItemInfo, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector } from '@xstate/react';
import Screen from 'components/Screen';
import { useStatechart } from 'state/Statechart';
import { menuSelector } from 'state/selectors';
import { Menu } from 'api/menu';
import MenuListItem from 'components/MenuListItem';
import Spacer from 'components/Spacer';

function MenuItem({ item }: ListRenderItemInfo<Menu>) {
  return <MenuListItem menu={item} />;
}

function MenuSpacer() {
  return <Spacer height={30} />;
}

const styles = StyleSheet.create({
  content: { flexGrow: 1 },
  list: { height: '100%' },
});

export default function Homepage() {
  const service = useStatechart();
  const menu = useSelector(service, menuSelector);
  return (
    <Screen testID="Homepage">
      <FlatList
        style={styles.list}
        contentContainerStyle={styles.content}
        renderItem={MenuItem}
        data={menu}
        ItemSeparatorComponent={MenuSpacer}
      />
    </Screen>
  );
}
