import React from 'react';
import Screen from 'components/Screen';
import Indent from 'components/Indent';
import { Title } from 'components/Text';
import Spacer from 'components/Spacer';

export default function Payment() {
  return (
    <Screen testID="Payment">
      <Indent width={16}>
        <Spacer height={20} />
        <Title>Next part of the payment funnel...</Title>
      </Indent>
    </Screen>
  );
}
