import React from 'react';
import { State } from 'xstate';
import { act, render, cleanup } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    value: 'error',
  }),
);

describe('Error', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByText } = renderAPI;
    expect(queryByText(/An error has occurred/i)).not.toBeNull();
  });
});
