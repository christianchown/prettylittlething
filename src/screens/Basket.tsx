import React, { useCallback } from 'react';
import { ListRenderItemInfo, StyleSheet, View } from 'react-native';
import Screen from 'components/Screen';
import { FlatList } from 'react-native-gesture-handler';
import Spacer from 'components/Spacer';
import BasketListItem, { BasketWithProduct } from 'components/BasketListItem';
import { basketSelector, productsSelector } from 'state/selectors';
import { useStatechart } from 'state/Statechart';
import { useSelector } from '@xstate/react';
import { findProduct } from 'api/product';
import Button from 'components/Button';
import { BasketTotal } from 'components/Text';
import { formatPrice } from 'lib/formatPrice';

function BasketItem({ item }: ListRenderItemInfo<BasketWithProduct>) {
  return <BasketListItem basket={item} />;
}

function BasketSpacer() {
  return <Spacer height={10} />;
}

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'space-between' },
  content: { flexGrow: 1 },
  list: {},
});

export default function Basket() {
  const service = useStatechart();
  const basket = useSelector(service, basketSelector);
  const products = useSelector(service, productsSelector);
  const basketWithProducts = basket
    .map((basketItem) => ({
      ...basketItem,
      product: products.find(findProduct(basketItem.id)),
    }))
    .filter((basketItem) => !!basketItem.product) as BasketWithProduct[];
  const toPayment = useCallback(
    () => service.send({ type: 'PAYMENT' }),
    [service],
  );
  const canPay = service.nextState({ type: 'PAYMENT' }).changed;
  const totalPrice = basketWithProducts.reduce(
    (acc, { product: { price }, quantity }) => acc + price * quantity,
    0,
  );
  return (
    <Screen testID="Basket">
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.content}
          renderItem={BasketItem}
          data={basketWithProducts}
          ItemSeparatorComponent={BasketSpacer}
        />
        <View>
          <Spacer height={20} />
          <BasketTotal testID="total">
            Bag total: £{formatPrice(totalPrice)}
          </BasketTotal>
          <Spacer height={20} />
          <Button disabled={!canPay} onPress={toPayment} testID="toPayment">
            PROCEED TO PAYMENT
          </Button>
        </View>
      </View>
    </Screen>
  );
}
