import React from 'react';
import { State } from 'xstate';
import { render, cleanup, act, fireEvent } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    context: {
      ...appMachine.initialState.context,
      products: [
        {
          id: 1,
          name: 'test product',
          colour: 'blue',
          price: 3.99,
          img: 'img.png',
        },
      ],
    },
    value: { catalog: 'products' },
  }),
);

describe('Catalog', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, queryByText } = renderAPI;
    expect(queryByTestId('Catalog')).not.toBeNull();
    expect(queryByTestId('view1')).not.toBeNull();
    expect(queryByText('test product')).not.toBeNull();
  });

  it('goes to the product page when you click on a product', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId, getByTestId } = renderAPI;
    await act(async () => fireEvent(getByTestId('view1'), 'onPress'));
    expect(queryByTestId('ProductPage')).not.toBeNull();
  });
});
