import React, { useCallback } from 'react';
import Screen from 'components/Screen';
import Indent from 'components/Indent';
import { Title } from 'components/Text';
import { useStatechart } from 'state/Statechart';
import Button from 'components/Button';
import Spacer from 'components/Spacer';

export default function Error() {
  const { send } = useStatechart();
  const retry = useCallback(() => send({ type: 'RETRY' }), [send]);
  return (
    <Screen testID="Error">
      <Indent width={16}>
        <Title>An error has occurred while loading the data</Title>
        <Spacer height={20} />
        <Button onPress={retry} testID="retry">
          RETRY
        </Button>
      </Indent>
    </Screen>
  );
}
