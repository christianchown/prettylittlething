import React from 'react';
import { ActivityIndicator } from 'react-native';
import Container from 'components/Container';
import Indent from 'components/Indent';
import Spacer from 'components/Spacer';

export default function Loading() {
  return (
    <Container>
      <Indent width={16}>
        <Spacer height={20} />
        <ActivityIndicator size="large" testID="spinner" />
      </Indent>
    </Container>
  );
}
