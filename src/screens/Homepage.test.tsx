import React from 'react';
import { State } from 'xstate';
import { render, fireEvent, act, cleanup } from '@testing-library/react-native';
import { appMachine } from 'state/appMachine';
import products from '__mocks__/mockProducts.json';
import menu from '__mocks__/mockMenu.json';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    context: { ...appMachine.initialState.context, products, menu },
    value: { catalog: 'homepage' },
  }),
);

describe('Homepage', () => {
  afterEach(cleanup);

  it('renders correctly', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { queryByTestId } = renderAPI;
    expect(queryByTestId('Homepage')).not.toBeNull();
    expect(queryByTestId('Catalog')).toBeNull();
  });

  it('goes to the catalog when you click a category', async () => {
    const renderAPI = render(<App state={state} />);
    await act(async () => renderAPI.update(<App state={state} />));
    const { getByTestId, queryByTestId } = renderAPI;
    await act(async () =>
      fireEvent(getByTestId('categoryNEW IN.NEW IN.View all'), 'onPress'),
    );
    await act(async () => renderAPI.update(<App state={state} />));
    expect(queryByTestId('Catalog')).not.toBeNull();
  });
});
