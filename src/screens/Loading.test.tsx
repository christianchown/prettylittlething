import React from 'react';
import { State } from 'xstate';
import { render, act, cleanup } from '@testing-library/react-native';
import jestFetchMock from 'jest-fetch-mock';
import { appMachine } from 'state/appMachine';
import App from '../App';

const state = appMachine.resolveState(
  State.create({
    ...appMachine.initialState,
    value: 'loading',
  }),
);

describe('Loading', () => {
  afterEach(cleanup);

  it('loads and then shows the homepage', async () => {
    const renderAPI = render(<App state={state} />);
    const { queryByTestId } = renderAPI;
    expect(queryByTestId('spinner')).not.toBeNull();
    await act(async () => renderAPI.update(<App state={state} />));
    expect(queryByTestId('spinner')).toBeNull();
    expect(queryByTestId('Homepage')).not.toBeNull();
  });

  it('loads and then shows the error page if initialLoad fails', async () => {
    jestFetchMock.mockResponse(async () => {
      throw new Error('error');
    });
    const renderAPI = render(<App state={state} />);
    const { queryByTestId } = renderAPI;
    expect(queryByTestId('spinner')).not.toBeNull();
    await act(async () => renderAPI.update(<App state={state} />));
    expect(queryByTestId('spinner')).toBeNull();
    expect(queryByTestId('Error')).not.toBeNull();
  });
});
