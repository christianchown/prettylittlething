import products from '__mocks__/mockProducts.json';
import { findProduct, httpToHttps, Product } from './product';

describe('findProduct', () => {
  it('should find products', () => {
    expect(products.some(findProduct(1))).toBe(true);
  });

  it('should not find non-existent products', () => {
    expect(products.some(findProduct(0))).toBe(false);
    expect(products.some(findProduct(-999))).toBe(false);
  });
});

describe('httpToHttps', () => {
  it('should change http:// to https://', () => {
    const source: Product = {
      id: 1,
      colour: 'red',
      price: 5,
      name: 'Test Product',
      img: 'http://some.url/image.png',
    };
    const expected: Product = {
      ...source,
      img: 'https://some.url/image.png',
    };
    expect(httpToHttps(source)).toEqual(expected);
  });

  it('should leave https:// unchanged', () => {
    const source: Product = {
      id: 1,
      colour: 'red',
      price: 5,
      name: 'Test Product',
      img: 'https://some.url/image.png',
    };
    expect(httpToHttps(source)).toEqual(source);
  });
});
