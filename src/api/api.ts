import { Menu } from './menu';
import { Product } from './product';

export async function initialLoad() {
  const [menu, products]: [Menu[], Product[]] = await Promise.all([
    fetch(
      'https://my-json-server.typicode.com/benirvingplt/products/menu',
    ).then((response) => response.json()),
    fetch(
      'https://my-json-server.typicode.com/benirvingplt/products/products',
    ).then((response) => response.json()),
  ]);
  return {
    menu,
    products,
  };
}
