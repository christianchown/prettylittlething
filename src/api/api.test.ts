import { initialLoad } from './api';
import products from '__mocks__/mockProducts.json';
import menu from '__mocks__/mockMenu.json';

describe('initialLoad', () => {
  it('should return data', async () => {
    const result = await initialLoad();
    expect(result.menu).toEqual(menu);
    expect(result.products).toEqual(products);
  });
});
