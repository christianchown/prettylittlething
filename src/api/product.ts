export type Product = {
  id: number;
  colour: string;
  name: string;
  price: number;
  img: string;
};

export const findProduct = (id: number) => (p: Product) => p.id === id;

export const httpToHttps = (p: Product) =>
  p.img.includes('http://')
    ? { ...p, img: p.img.replace('http://', 'https://') }
    : p;
