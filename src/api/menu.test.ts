import menu from '__mocks__/mockMenu.json';
import { findMenu } from './menu';

describe('findMenu', () => {
  it('should find menu items', () => {
    expect(menu.some(findMenu('NEW IN', 'NEW IN', 'View all'))).toBe(true);
  });

  it('should not find non-existent menu items', () => {
    expect(menu.some(findMenu('NEW IN', 'NEW IN', 'Nope'))).toBe(false);
    expect(menu.some(findMenu('NEW IN', 'Nope', 'View all'))).toBe(false);
    expect(menu.some(findMenu('Nope', 'NEW IN', 'View all'))).toBe(false);
  });
});
