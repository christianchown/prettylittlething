export type Category = {
  name: string;
  categories: string[];
};

export type Menu = {
  name: string;
  img: string;
  children: Category[];
};

const findCategory = (name: string, category: string) => (c: Category) =>
  c.name === name && c.categories.includes(category);

export const findMenu =
  (parent: string, child: string, category: string) => (m: Menu) =>
    m.name === parent && m.children.some(findCategory(child, category));
