import React, { useMemo } from 'react';
import { View } from 'react-native';

type Props = { height: number };

export default function Spacer({ height }: Props) {
  const style = useMemo(() => ({ height }), [height]);
  return <View style={style} />;
}
