import { useSelector } from '@xstate/react';
import React, { useCallback } from 'react';
import { Pressable, StyleSheet, View, Text } from 'react-native';
import { Basket } from 'state/basket';
import { basketSelector } from 'state/selectors';
import { useStatechart } from 'state/Statechart';
import Centred from './Centred';
import Indent from './Indent';
import { TextProp, Title } from './Text';

const styles = StyleSheet.create({
  view: {
    flexDirection: 'row',
    paddingVertical: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  basket: {
    paddingHorizontal: 5,
    borderWidth: 1,
    borderColor: 'silver',
    backgroundColor: 'white',
    paddingVertical: 5,
    flexDirection: 'row',
  },
  disabled: {
    opacity: 0.5,
  },
  icon: {},
  quantity: {
    fontWeight: '600',
    color: 'black',
  },
});

type Props = {
  children: TextProp;
};

export const sumBasket = (acc: number, { quantity }: Basket) => acc + quantity;

export default function Header({ children }: Props) {
  const service = useStatechart();
  const { send } = service;
  const basket = useSelector(service, basketSelector);
  const canCheckout = service.nextState({ type: 'CHECKOUT' }).changed;
  const checkout = useCallback(() => send({ type: 'CHECKOUT' }), [send]);
  return (
    <Indent width={15}>
      <View style={styles.view}>
        <Centred>
          <Title>{children}</Title>
        </Centred>
        <Pressable onPress={checkout} disabled={!canCheckout} testID="checkout">
          <View
            style={
              !canCheckout ? [styles.basket, styles.disabled] : styles.basket
            }>
            <Text style={styles.icon}>🛒</Text>
            <Text style={styles.quantity} testID="quantity">
              {basket.reduce(sumBasket, 0)}
            </Text>
          </View>
        </Pressable>
      </View>
    </Indent>
  );
}
