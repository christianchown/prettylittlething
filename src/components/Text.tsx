import { StyleSheet, Text } from 'react-native';
import React from 'react';
import { ReactElement } from 'react';
import { TextProps } from 'react-native';

type StringOrText = string | ReactElement<TextProps>;
export type TextProp = StringOrText | StringOrText[];

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  productTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    flex: 1,
    marginBottom: 10,
    textAlign: 'center',
    color: 'darkgreen',
  },
  productDetail: {
    fontSize: 16,
    paddingVertical: 4,
  },
  bold: {
    fontWeight: 'bold',
  },
  basketTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  basketTotal: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'darkgreen',
    textAlign: 'center',
  },
  categoryTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'darkgreen',
    textAlign: 'center',
  },
  categoryName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

type Props = {
  children: TextProp;
  testID?: string;
};

export function Title({ children, ...rest }: Props) {
  return (
    <Text style={styles.title} {...rest}>
      {children}
    </Text>
  );
}

export function ProductTitle({ children, ...rest }: Props) {
  return (
    <Text style={styles.productTitle} {...rest}>
      {children}
    </Text>
  );
}

export function ProductDetail({ children, ...rest }: Props) {
  return (
    <Text style={styles.productDetail} {...rest}>
      {children}
    </Text>
  );
}

export function CategoryTitle({ children, ...rest }: Props) {
  return (
    <Text style={styles.categoryTitle} {...rest}>
      {children}
    </Text>
  );
}
export function CategoryName({ children, ...rest }: Props) {
  return (
    <Text style={styles.categoryName} {...rest}>
      {children}
    </Text>
  );
}
export function Bold({ children, ...rest }: Props) {
  return (
    <Text style={styles.bold} {...rest}>
      {children}
    </Text>
  );
}

export function BasketTitle({ children, ...rest }: Props) {
  return (
    <Text style={styles.basketTitle} {...rest}>
      {children}
    </Text>
  );
}

export function BasketTotal({ children, ...rest }: Props) {
  return (
    <Text style={styles.basketTotal} {...rest}>
      {children}
    </Text>
  );
}
