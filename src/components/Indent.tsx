import React, { ReactNode, useMemo } from 'react';
import { Dimensions, View } from 'react-native';

const { width: windowWidth } = Dimensions.get('window');
const defaultWidth = 375;
const widthScale = windowWidth / defaultWidth;

type Props = {
  width: number;
  children: ReactNode;
  flexed?: boolean;
};

export default function Indent({ children, width, flexed = false }: Props) {
  const style = useMemo(
    () => ({
      paddingHorizontal: width * widthScale,
      ...(flexed ? { flex: 1 } : {}),
    }),
    [width, flexed],
  );
  return <View style={style}>{children}</View>;
}
