import React from 'react';
import { View, Pressable, StyleSheet } from 'react-native';
import { useStatechart } from 'state/Statechart';
import ImageWithPlaceholder from './ImageWithPlaceholder';
import { CategoryTitle, CategoryName } from './Text';
import { Category, Menu } from 'api/menu';

const styles = StyleSheet.create({
  wrap: {
    height: 300,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  child: {},
  categories: {
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1,
    flexWrap: 'wrap',
  },
  categoryWrap: {
    marginVertical: 5,
    flex: 1,
    flexBasis: 1,
    borderWidth: 2,
    borderColor: 'black',
    padding: 10,
  },
});

function MenuChild({
  child: { name, categories },
  menu,
}: {
  child: Category;
  menu: Menu;
}) {
  const { send } = useStatechart();
  const viewCategory = (category: string) => () => {
    send({ type: 'VIEW_CATEGORY', parent: menu.name, child: name, category });
  };
  return (
    <View style={styles.child}>
      {name !== menu.name && <CategoryTitle>{name}</CategoryTitle>}
      <View style={styles.categories}>
        {categories.map((category) => (
          <Pressable
            key={`category${menu.name}.${name}.${category}`}
            testID={`category${menu.name}.${name}.${category}`}
            onPress={viewCategory(category)}>
            <View style={styles.categoryWrap}>
              <CategoryName>{category}</CategoryName>
            </View>
          </Pressable>
        ))}
      </View>
    </View>
  );
}

export default React.memo(function MenuListItem({ menu }: { menu: Menu }) {
  const { children, name, img: uri } = menu;
  return (
    <>
      <CategoryTitle>{name}</CategoryTitle>
      <View style={styles.wrap}>
        <ImageWithPlaceholder style={styles.image} source={{ uri }} />
      </View>
      {children.map((child) => (
        <MenuChild
          key={`child${menu.name}.${child.name}`}
          child={child}
          menu={menu}
        />
      ))}
    </>
  );
});
