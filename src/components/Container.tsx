import React, { ReactNode } from 'react';
import { SafeAreaView, StyleSheet, StatusBar, View } from 'react-native';

const styles = StyleSheet.create({
  container: { flex: 1 },
  content: {
    flex: 1,
  },
});

export default function Container({ children }: { children: ReactNode }) {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <View style={styles.content}>{children}</View>
    </SafeAreaView>
  );
}
