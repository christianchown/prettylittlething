import React from 'react';
import { ImageBackground, ImageBackgroundProps } from 'react-native';

export default function ImageWithPlaceholder(props: ImageBackgroundProps) {
  return <ImageBackground resizeMode="contain" {...props} />;
}
