import React, { ReactNode } from 'react';
import { StyleSheet, View } from 'react-native';
import Indent from './Indent';
import Spacer from './Spacer';

const styles = StyleSheet.create({
  screen: { flex: 1 },
});

export default function Screen({
  children,
  testID,
}: {
  children: ReactNode;
  testID: string;
}) {
  return (
    <View style={styles.screen} testID={testID}>
      <Indent width={15} flexed>
        <Spacer height={10} />
        {children}
        <Spacer height={10} />
      </Indent>
    </View>
  );
}
