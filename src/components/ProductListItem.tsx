import React, { useCallback } from 'react';
import { View, Pressable, StyleSheet, Dimensions } from 'react-native';
import { Product } from 'api/product';
import { useStatechart } from 'state/Statechart';
import ImageWithPlaceholder from './ImageWithPlaceholder';
import { Bold, ProductDetail, ProductTitle } from './Text';
import { formatPrice } from 'lib/formatPrice';

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    height: 300,
  },
  textWrap: {
    flexDirection: 'column',
    paddingLeft: 10,
  },
  image: {
    width: windowWidth * 0.5,
    height: '100%',
  },
});

export default React.memo(function ProductListItem({
  product: { id, colour, name, price, img },
}: {
  product: Product;
}) {
  const { send } = useStatechart();
  const viewProduct = useCallback(
    () => send({ type: 'VIEW_PRODUCT', id }),
    [send, id],
  );
  return (
    <Pressable onPress={viewProduct} testID={`view${id}`}>
      <ProductTitle>{name}</ProductTitle>
      <View style={styles.wrap}>
        <ImageWithPlaceholder style={styles.image} source={{ uri: img }} />
        <View style={styles.textWrap}>
          <ProductDetail>
            Colour: <Bold>{colour}</Bold>
          </ProductDetail>
          <ProductDetail>
            Price: <Bold>£{formatPrice(price)}</Bold>
          </ProductDetail>
        </View>
      </View>
    </Pressable>
  );
});
