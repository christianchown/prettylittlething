import React, { ReactNode } from 'react';
import { StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
  centred: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default function Centred({ children }: { children: ReactNode }) {
  return <View style={styles.centred}>{children}</View>;
}
