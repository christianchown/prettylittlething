import React, { ReactNode } from 'react';
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'darkgreen',
    borderWidth: 4,
    borderColor: 'darkgreen',
  },
  viewDisabled: {
    backgroundColor: 'gainsboro',
    borderWidth: 4,
    borderColor: 'lightgray',
  },
  text: {
    color: 'white',
    paddingHorizontal: 20,
    paddingVertical: 8,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
});

type Props = {
  children: ReactNode;
  disabled?: boolean;
  testID: string;
  onPress: () => void;
};

export default function Button({
  children,
  onPress,
  testID,
  disabled = false,
}: Props) {
  return (
    <TouchableOpacity onPress={onPress} disabled={disabled} testID={testID}>
      <View style={disabled ? styles.viewDisabled : styles.view}>
        <Text style={styles.text}>{children}</Text>
      </View>
    </TouchableOpacity>
  );
}
