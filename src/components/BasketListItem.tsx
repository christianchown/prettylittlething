import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { Platform, Pressable, StyleSheet, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { Basket } from 'state/basket';
import { Product } from 'api/product';
import { Bold, ProductDetail, BasketTitle } from './Text';
import ImageWithPlaceholder from './ImageWithPlaceholder';
import { formatPrice } from 'lib/formatPrice';
import Spacer from './Spacer';
import Indent from './Indent';
import { useStatechart } from 'state/Statechart';

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: 'black',
  },
  wrap: {
    flexDirection: 'row',
  },
  quantity: {
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    borderWidth: 1,
    borderColor: 'black',
  },
  textWrap: {
    paddingLeft: 10,
  },
  imageWrap: {
    width: '50%',
    height: 100,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  bin: {
    alignItems: 'flex-end',
    flexGrow: 1,
  },
});

export type BasketWithProduct = Basket & { product: Product };

export default React.memo(function BasketListItem({
  basket: {
    id,
    quantity,
    product: { name, img, price, colour },
  },
}: {
  basket: BasketWithProduct;
}) {
  const [quantityAdjust, setQuantityAdjust] = useState(false);
  const adjustQuantity = useCallback(() => setQuantityAdjust(true), []);
  const pickerRef = useRef<Picker<number>>(null);
  useEffect(() => {
    if (quantityAdjust && Platform.OS === 'android') {
      pickerRef.current?.focus();
    }
  }, [quantityAdjust]);
  const { send } = useStatechart();
  const { changeQuantity, removeFromBasket, viewProduct } = useMemo(
    () => ({
      changeQuantity: (newQuantity: number) => {
        send({
          type: 'UPDATE_BASKET',
          payload: { id, quantity: newQuantity },
        });
        setQuantityAdjust(false);
      },
      removeFromBasket: () => {
        send({
          type: 'UPDATE_BASKET',
          payload: { id, quantity: 0 },
        });
        setQuantityAdjust(false);
      },
      viewProduct: () =>
        send({
          type: 'VIEW_PRODUCT',
          id,
        }),
    }),
    [id, send],
  );
  return (
    <View style={styles.container} testID={`basket${id}`}>
      <Indent width={10}>
        <Spacer height={5} />
        <Pressable onPress={viewProduct} testID={`viewTitle${id}`}>
          <BasketTitle>{name}</BasketTitle>
        </Pressable>
        <Spacer height={10} />
        <View style={styles.wrap}>
          <View style={styles.imageWrap}>
            <Pressable onPress={viewProduct} testID={`view${id}`}>
              <ImageWithPlaceholder
                style={styles.image}
                source={{ uri: img }}
              />
            </Pressable>
          </View>
          <View style={styles.textWrap}>
            <ProductDetail>{colour}</ProductDetail>
            {quantityAdjust ? (
              <Picker
                ref={pickerRef}
                style={styles.picker}
                selectedValue={quantity}
                onValueChange={changeQuantity}
                testID={`picker${id}`}>
                {Array.from({ length: 20 }).map((_, i) => (
                  <Picker.Item
                    key={`Picker${i}`}
                    label={(i + 1).toString()}
                    value={i + 1}
                  />
                ))}
              </Picker>
            ) : (
              <Pressable onPress={adjustQuantity} testID={`adjust${id}`}>
                <View style={styles.quantity}>
                  <ProductDetail testID={`quantity${id}`}>
                    {quantity.toString()}
                  </ProductDetail>
                </View>
              </Pressable>
            )}
            <ProductDetail> @ £{formatPrice(price)}ea</ProductDetail>
            <ProductDetail>
              <Bold>£{formatPrice(price * quantity)}</Bold>
            </ProductDetail>
          </View>
          <View style={styles.bin}>
            <Pressable onPress={removeFromBasket} testID={`bin${id}`}>
              <ProductDetail>🗑️</ProductDetail>
            </Pressable>
          </View>
        </View>
        <Spacer height={5} />
      </Indent>
    </View>
  );
});
