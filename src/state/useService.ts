import { useEffect, useMemo } from 'react';
import { EventObject, interpret, StateMachine, State } from 'xstate';

export function useService<Context, Schema, Event extends EventObject>(
  machine: StateMachine<Context, Schema, Event>,
  initialState?: State<Context, Event>,
) {
  const service = useMemo(
    () => interpret(machine).start(initialState),
    [machine, initialState],
  );
  useEffect(
    () => () => {
      service.stop();
    },
    [service],
  );
  return service;
}
