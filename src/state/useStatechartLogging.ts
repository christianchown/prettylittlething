import { useEffect } from 'react';
import { EventObject, Interpreter } from 'xstate';

export function useStatechartLogging<
  Context,
  Schema,
  Event extends EventObject,
>(service: Interpreter<Context, Schema, Event>, enabled: boolean) {
  useEffect(() => {
    const { unsubscribe } = service.subscribe((state) => {
      if (enabled && state.changed) {
        console.log(state.value, state.event /*, state.context*/);
      }
    });
    return unsubscribe;
  }, [service, enabled]);
}
