import { State } from 'xstate';
import { appMachine } from './appMachine';
import products from '__mocks__/mockProducts.json';
import menu from '__mocks__/mockMenu.json';

const { initialState: defaultConfig } = appMachine;

describe('appMachine', () => {
  it('should start in a loading state with an empty context', () => {
    expect(defaultConfig.value).toEqual('loading');
    expect(defaultConfig.context.basket).toEqual([]);
    expect(defaultConfig.context.products).toEqual([]);
    expect(defaultConfig.context.menu).toEqual([]);
  });

  describe('loading', () => {
    it('should populate the context with transformed URLs if it loads correctly', () => {
      let state = appMachine.resolveState(State.create({ ...defaultConfig }));
      state = appMachine.transition(state, {
        type: 'done.invoke.loading',
        data: { products, menu },
      });
      expect(state.context.products.length).toEqual(products.length);
      state.context.products.forEach((stateProduct, i) => {
        expect(stateProduct).toEqual(
          expect.objectContaining({
            ...products[i],
            img: products[i].img.replace('http:', 'https:'),
          }),
        );
      });
      expect(state.context.menu).toEqual(menu);
    });

    it('should go to the homepage if it loads correctly', () => {
      let state = appMachine.resolveState(State.create({ ...defaultConfig }));
      state = appMachine.transition(state, {
        type: 'done.invoke.loading',
        data: { products, menu },
      });
      expect(state.value).toEqual({ catalog: 'homepage' });
    });

    it('should go to error if an error occurs', () => {
      let state = appMachine.resolveState(State.create({ ...defaultConfig }));
      state = appMachine.transition(state, {
        type: 'error.platform.loading',
      } as any);
      expect(state.value).toEqual('error');
    });
  });

  describe('homepage', () => {
    const initialState = {
      ...defaultConfig,
      context: { products, menu, basket: [] },
      value: { catalog: 'homepage' },
    };

    it('should go to the catalog if the user clicks on a category', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, {
        type: 'VIEW_CATEGORY',
        parent: 'NEW IN',
        child: 'PLT RANGES',
        category: 'New in plus',
      });
      expect(state.value).toEqual({ catalog: 'products' });
    });

    it("should not go to the catalog if the category doesn't exist", () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, {
        type: 'VIEW_CATEGORY',
        parent: 'NEW IN',
        child: 'PLT RANGES',
        category: '***does not exist***',
      });
      expect(state.changed).toBe(false);
    });

    it('should not go to checkout if your basket is empty', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.changed).toBe(false);
    });

    it('should go to checkout if your basket is not empty', () => {
      let state = appMachine.resolveState(
        State.create({
          ...initialState,
          context: {
            ...initialState.context,
            basket: [{ id: 1, quantity: 1 }],
          },
        }),
      );
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.value).toEqual({ checkout: 'basket' });
    });
  });

  describe('products', () => {
    const initialState = {
      ...defaultConfig,
      context: { products, menu, basket: [] },
      value: { catalog: 'products' },
    };

    it('should go to the product page if you view a product', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'VIEW_PRODUCT', id: 1 });
      expect(state.value).toEqual({ catalog: 'product' });
    });

    it('should not go to the product page if you attempt to view a non-existent product', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, {
        type: 'VIEW_PRODUCT',
        id: -99999,
      });
      expect(state.changed).toBe(false);
    });

    it('should not go to checkout if your basket is empty', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.changed).toBe(false);
    });

    it('should go to checkout if your basket is not empty', () => {
      let state = appMachine.resolveState(
        State.create({
          ...initialState,
          context: {
            ...initialState.context,
            basket: [{ id: 1, quantity: 1 }],
          },
        }),
      );
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.value).toEqual({ checkout: 'basket' });
    });
  });

  describe('product', () => {
    const initialState = {
      ...defaultConfig,
      context: { products, menu, basket: [] },
      value: { catalog: 'product' },
    };

    it('should go back to the catalog', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'BACK' });
      expect(state.value).toEqual({ catalog: 'products' });
    });

    it('should add a new product to the basket', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'ADD_TO_BASKET', id: 1 });
      expect(state.context.basket).toEqual([{ id: 1, quantity: 1 }]);
    });

    it('should not add a non-existent product to the basket', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, {
        type: 'ADD_TO_BASKET',
        id: -9999,
      });
      expect(state.context.basket).toEqual([]);
    });

    it('should update quantity of a product already in the basket', () => {
      let state = appMachine.resolveState(
        State.create({
          ...initialState,
          context: {
            ...initialState.context,
            basket: [{ id: 1, quantity: 1 }],
          },
        }),
      );
      state = appMachine.transition(state, { type: 'ADD_TO_BASKET', id: 1 });
      expect(state.context.basket).toEqual([{ id: 1, quantity: 2 }]);
    });

    it('should not go to checkout if there are no products in the basket', () => {
      let state = appMachine.resolveState(State.create(initialState));

      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.changed).toBe(false);
    });

    it('should go to checkout if there are products in the basket', () => {
      let state = appMachine.resolveState(
        State.create({
          ...initialState,
          context: {
            ...initialState.context,
            basket: [{ id: 1, quantity: 1 }],
          },
        }),
      );
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.value).toEqual({ checkout: 'basket' });
    });
  });

  describe('checkout', () => {
    const initialState = {
      ...defaultConfig,
      context: { products, menu, basket: [{ id: 1, quantity: 1 }] },
      value: { checkout: 'basket' },
    };

    it('should go back to the catalog when coming from the catalog', () => {
      let state = appMachine.resolveState(
        State.create({ ...initialState, value: { catalog: 'products' } }),
      );
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.value).toEqual({ checkout: 'basket' });
      state = appMachine.transition(state, { type: 'BACK' });
      expect(state.value).toEqual({ catalog: 'products' });
    });

    it('should go back to a product page when coming from a product page', () => {
      let state = appMachine.resolveState(
        State.create({ ...initialState, value: { catalog: 'products' } }),
      );
      state = appMachine.transition(state, { type: 'VIEW_PRODUCT', id: 1 });
      state = appMachine.transition(state, { type: 'CHECKOUT' });
      expect(state.value).toEqual({ checkout: 'basket' });
      state = appMachine.transition(state, { type: 'BACK' });
      expect(state.value).toEqual({ catalog: 'product' });
    });

    it('should go back to the payment page', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'PAYMENT' });
      expect(state.value).toEqual({ checkout: 'payment' });
    });

    it('should go back to the basket from payment', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'PAYMENT' });
      state = appMachine.transition(state, { type: 'BACK' });
      expect(state.value).toEqual({ checkout: 'basket' });
    });

    it('should go to the product page if you view a product', () => {
      let state = appMachine.resolveState(State.create(initialState));
      state = appMachine.transition(state, { type: 'VIEW_PRODUCT', id: 1 });
      expect(state.value).toEqual({ checkout: 'viewProduct' });
    });
  });

  describe('error', () => {
    it('should be able to retry loading', () => {
      let state = appMachine.resolveState(
        State.create({ ...defaultConfig, value: 'error' }),
      );
      state = appMachine.transition(state, { type: 'RETRY' });
      expect(state.value).toEqual('loading');
    });
  });
});
