export type Basket = {
  id: number;
  quantity: number;
};

export const notInBasket = (id: number) => (basket: Basket) => basket.id !== id;

export const replaceInBasket = (replacement: Basket) => (basket: Basket) =>
  basket.id === replacement.id ? replacement : basket;

export const addOneToBasket = (id: number) => (basket: Basket) =>
  basket.id === id ? { ...basket, quantity: basket.quantity + 1 } : basket;
