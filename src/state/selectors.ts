import { State } from 'xstate';
import { Context, Event } from './appMachine';

export const stateValueSelector = (state: State<Context, Event>) => state.value;

export const eventSelector = (state: State<Context, Event>) => state.event;

export const historySelector = (state: State<Context, Event>) =>
  state.historyValue;

export const productsSelector = (state: State<Context, Event>) =>
  state.context.products;

export const menuSelector = (state: State<Context, Event>) =>
  state.context.menu;

export const basketSelector = (state: State<Context, Event>) =>
  state.context.basket;
