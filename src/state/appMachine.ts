import { assign, createMachine, DoneInvokeEvent } from 'xstate';
import { findProduct, httpToHttps, Product } from 'api/product';
import { findMenu, Menu } from 'api/menu';
import { initialLoad } from 'api/api';
import { addOneToBasket, Basket, notInBasket, replaceInBasket } from './basket';

const initialContext = {
  menu: [] as Menu[],
  products: [] as Product[],
  basket: [] as Basket[],
};
export type Context = typeof initialContext;

type Depromisified<T> = T extends Promise<infer U> ? U : T;

type AddToBasket = {
  type: 'ADD_TO_BASKET';
  id: number;
};
type ViewCategory = {
  type: 'VIEW_CATEGORY';
  parent: string;
  child: string;
  category: string;
};
type ViewProduct = {
  type: 'VIEW_PRODUCT';
  id: number;
};
type UpdateBasket = {
  type: 'UPDATE_BASKET';
  payload: { id: number; quantity: number };
};
type Back = { type: 'BACK' };
type Retry = { type: 'RETRY' };
type Checkout = { type: 'CHECKOUT' };
type Payment = { type: 'PAYMENT' };
type InvokeLoading = {
  type: 'done.invoke.loading';
  data: Depromisified<ReturnType<typeof initialLoad>>;
};
export type Event =
  | AddToBasket
  | UpdateBasket
  | ViewCategory
  | ViewProduct
  | Back
  | Retry
  | Checkout
  | Payment
  | InvokeLoading;

const actions = {
  assignValues: assign<Context, DoneInvokeEvent<InvokeLoading['data']>>(
    (_, { data: { menu, products } }) => ({
      menu,
      products: products.map(httpToHttps),
    }),
  ),
  addToBasket: assign<Context, AddToBasket>(({ basket }, { id }) => ({
    basket: basket.every(notInBasket(id))
      ? [...basket, { id, quantity: 1 }]
      : basket.map(addOneToBasket(id)),
  })),
  updateBasket: assign<Context, UpdateBasket>(
    ({ basket, products }, { payload: { id, quantity } }) => ({
      basket:
        quantity === 0
          ? basket.filter(notInBasket(id))
          : !products.some(findProduct(id))
          ? basket
          : basket.every(notInBasket(id))
          ? [...basket, { id, quantity }]
          : basket.map(replaceInBasket({ id, quantity })),
    }),
  ),
};

const guards = {
  basketNotEmpty: ({ basket }: Context) => basket.length > 0,
  productExists: ({ products }: Context, { id }: ViewProduct | AddToBasket) =>
    products.some(findProduct(id)),
  categoryExists: (
    { menu }: Context,
    { parent, child, category }: ViewCategory,
  ) => menu.some(findMenu(parent, child, category)),
};

export const appMachine = createMachine<Context, Event>({
  id: 'app',
  context: { ...initialContext },
  initial: 'loading',
  states: {
    loading: {
      invoke: {
        id: 'loading',
        src: initialLoad,
        onDone: { target: 'catalog', actions: actions.assignValues },
        onError: 'error',
      },
    },
    catalog: {
      initial: 'homepage',
      on: {
        CHECKOUT: { target: '#app.checkout', cond: guards.basketNotEmpty },
      },
      states: {
        homepage: {
          on: {
            VIEW_CATEGORY: { target: 'products', cond: guards.categoryExists },
          },
        },
        products: {
          on: {
            BACK: 'homepage',
            VIEW_PRODUCT: { target: 'product', cond: guards.productExists },
          },
        },
        product: {
          on: {
            BACK: 'products',
            ADD_TO_BASKET: {
              actions: actions.addToBasket,
              cond: guards.productExists,
            },
          },
        },
        history: { type: 'history' },
      },
    },
    checkout: {
      initial: 'basket',
      states: {
        basket: {
          on: {
            BACK: '#app.catalog.history',
            PAYMENT: { target: 'payment', cond: guards.basketNotEmpty },
            UPDATE_BASKET: { actions: actions.updateBasket },
            VIEW_PRODUCT: {
              target: 'viewProduct',
              cond: guards.productExists,
            },
          },
        },
        viewProduct: {
          on: {
            BACK: 'basket',
            ADD_TO_BASKET: {
              actions: actions.addToBasket,
              cond: guards.productExists,
            },
          },
        },
        payment: { on: { BACK: 'basket' } },
      },
    },
    error: { on: { RETRY: 'loading' } },
  },
});
