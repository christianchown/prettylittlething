import { addOneToBasket, notInBasket, replaceInBasket } from './basket';

describe('notInBasket', () => {
  it('should be false for a matching id', () => {
    expect(notInBasket(1)({ id: 1, quantity: 1 })).toBe(false);
  });

  it('should be true for a mismatched id', () => {
    expect(notInBasket(1)({ id: 2, quantity: 1 })).toBe(true);
  });
});

describe('replaceInBasket', () => {
  it('should return the same for matching details', () => {
    expect(
      replaceInBasket({ id: 1, quantity: 1 })({ id: 1, quantity: 1 }),
    ).toEqual({ id: 1, quantity: 1 });
  });

  it('should return the same for a mismatched id', () => {
    expect(
      replaceInBasket({ id: 2, quantity: 1 })({ id: 1, quantity: 1 }),
    ).toEqual({ id: 1, quantity: 1 });
  });

  it('should replace a matched id', () => {
    expect(
      replaceInBasket({ id: 1, quantity: 10 })({ id: 1, quantity: 1 }),
    ).toEqual({ id: 1, quantity: 10 });
  });
});

describe('addOneToBasket', () => {
  it('should add to a matching id', () => {
    expect(addOneToBasket(1)({ id: 1, quantity: 1 })).toEqual({
      id: 1,
      quantity: 2,
    });
  });

  it('should leave a mismatched id', () => {
    expect(addOneToBasket(1)({ id: 2, quantity: 1 })).toEqual({
      id: 2,
      quantity: 1,
    });
  });
});
