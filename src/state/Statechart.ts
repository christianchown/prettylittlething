import { createContext, useContext } from 'react';
import { Interpreter } from 'xstate';
import { Context, Event } from './appMachine';

const stateContext = createContext<
  Interpreter<Context, any, Event> | undefined
>(undefined);

export function useStatechart() {
  const context = useContext(stateContext);
  if (typeof context === 'undefined') {
    throw new Error(
      'useStatechart must be used within a <StateMachineProvider />',
    );
  }
  return context;
}

export const StateMachineProvider = stateContext.Provider;
