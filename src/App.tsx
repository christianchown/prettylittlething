import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { State } from 'xstate';
import { appMachine, Context, Event } from 'state/appMachine';
import { StateMachineProvider } from 'state/Statechart';
import { useStatechartLogging } from 'state/useStatechartLogging';
import { useNavigationReady, navigationRef } from 'lib/useNavigationStack';
import { useService } from 'state/useService';
import Router from './Router';
import Container from 'components/Container';
import Header from 'components/Header';

const doLogging = __DEV__ && process.env.NODE_ENV !== 'test';

export default function App({ state }: { state?: State<Context, Event> }) {
  const service = useService(appMachine, state);
  useStatechartLogging(service, doLogging);
  const onReady = useNavigationReady();
  return (
    <StateMachineProvider value={service}>
      <NavigationContainer ref={navigationRef} onReady={onReady}>
        <Container>
          <Header>PRETTY LITTLE THING</Header>
          <Router />
        </Container>
      </NavigationContainer>
    </StateMachineProvider>
  );
}
