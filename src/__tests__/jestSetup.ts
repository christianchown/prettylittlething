import jestFetchMock from 'jest-fetch-mock';
import products from '__mocks__/mockProducts.json';
import menu from '__mocks__/mockMenu.json';

jest.useFakeTimers();

jestFetchMock.enableMocks();

jestFetchMock.mockResponse(async ({ url }: Request) => {
  if (url.includes('/products/products')) {
    const [, indexString] = url.split('/products/products', 2);
    return {
      body: JSON.stringify(
        indexString ? products[parseInt(indexString, 10)] || {} : products,
      ),
    };
  }
  if (url.includes('/products/menu')) {
    return { body: JSON.stringify(menu) };
  }
  return { status: 404, body: `Unknown request - ${url}` };
});

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

jest.mock('@react-navigation/core', () => {
  return {
    ...jest.requireActual('@react-navigation/core'),
    useNavigation: () => ({
      setOptions: jest.fn(),
    }),
  };
});
