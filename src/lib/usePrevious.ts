import { useRef, useEffect } from 'react';

export function usePrevious<T>(value: T) {
  const ref = useRef<T | undefined>(undefined);
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}

export function usePreviousIfDefined<T>(value: T) {
  const ref = useRef<T | undefined>(undefined);
  useEffect(() => {
    if (value !== undefined) {
      ref.current = value;
    }
  }, [value]);
  return ref.current;
}
