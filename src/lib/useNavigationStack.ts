import React, { useEffect, createRef, useCallback } from 'react';
import {
  StackActions,
  NavigationContainerRef,
  NavigationState,
  CommonActions,
} from '@react-navigation/native';
import { usePrevious } from './usePrevious';

export const navigationRef: React.RefObject<NavigationContainerRef<any>> =
  createRef();

export const navigationReady: React.MutableRefObject<boolean | null> =
  createRef();

export function useNavigationReady() {
  return useCallback(() => (navigationReady.current = true), []);
}

const emptyArray = [] as string[];

const stringsDiffer = (b: string[]) => (a: string, index: number) =>
  a !== b[index];

export const stringArraysEqual = (a: string[], b: string[]) =>
  a.length === b.length && !a.some(stringsDiffer(b));

const matchingRoute =
  (screen: string) =>
  ({ name }: { name: string }) =>
    name === screen;

export const keysForState = (
  state: NavigationState,
  screen: string,
): [string | undefined, string | undefined] => {
  const route = state.routes.find(matchingRoute(screen));
  if (route) {
    return [state.key, route.key];
  }
  const activeRoute = state.routes[state.index];
  if (activeRoute && activeRoute.state) {
    return keysForState(activeRoute.state as NavigationState, screen);
  }
  return [undefined, undefined];
};

export function useNavigationStack(getStack: () => string[]) {
  const newStack = getStack();
  if (newStack.length === 0) {
    throw new Error('useNavigationStack has no screens to render');
  }
  const previousStack =
    usePrevious(
      navigationRef.current && navigationReady.current ? newStack : emptyArray,
    ) || emptyArray;
  useEffect(() => {
    if (
      !stringArraysEqual(newStack, previousStack) &&
      navigationRef.current &&
      navigationReady.current
    ) {
      const [newHead, ...newTail] = newStack;
      const [previousHead, ...previousTail] = previousStack;
      const rootState = navigationRef.current.getRootState();
      const current = navigationRef.current.getCurrentRoute()?.name || '';
      if (stringArraysEqual(newStack, previousTail)) {
        const [target, source] = keysForState(rootState, previousHead);
        if (current !== newHead && target && source) {
          navigationRef.current.dispatch({
            ...CommonActions.goBack(),
            source,
            target,
          });
        }
      } else if (stringArraysEqual(previousStack, newTail)) {
        if (current !== newHead) {
          navigationRef.current.dispatch(StackActions.push(newHead));
        }
      } else if (
        previousStack.length === newStack.length &&
        stringArraysEqual(newTail, previousTail)
      ) {
        if (current !== newHead) {
          navigationRef.current.dispatch(StackActions.replace(newHead));
        }
      } else {
        const [target, source] = keysForState(rootState, previousHead);
        if (previousTail.length) {
          navigationRef.current.dispatch({
            ...StackActions.popToTop(),
            target,
            source,
          });
        }
        if (newTail.length) {
          if (process.env.NODE_ENV !== 'test') {
            console.warn(
              `invalid navigation stack change from ${JSON.stringify(
                previousStack,
              )} to ${JSON.stringify(newStack)}`,
            );
          }
        }
        navigationRef.current.dispatch(StackActions.replace(newHead));
      }
    }
  }, [newStack, previousStack]);
}
