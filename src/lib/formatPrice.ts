export function formatPrice(n: number) {
  return n.toFixed(2);
}
