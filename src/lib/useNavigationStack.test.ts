import { renderHook } from '@testing-library/react-hooks';
import { NavigationState } from '@react-navigation/native';
import {
  useNavigationStack,
  navigationRef,
  stringArraysEqual,
  keysForState,
  navigationReady,
} from './useNavigationStack';

describe('keysForState', () => {
  it('should return undefined for a screen that doesnt exist', () => {
    expect(
      keysForState(
        {
          stale: false,
          type: 'stack',
          key: 'stack1',
          index: 0,
          routeNames: ['LoggedIn', 'LoggedOut'],
          routes: [{ name: 'LoggedIn', key: 'LoggedIn1' }],
        },
        'MissingScreen',
      ),
    ).toEqual([undefined, undefined]);
  });

  it('should return the keys for a screen in the root state', () => {
    expect(
      keysForState(
        {
          stale: false,
          type: 'stack',
          key: 'stack1',
          index: 0,
          routeNames: ['LoggedIn', 'LoggedOut'],
          routes: [{ name: 'LoggedIn', key: 'LoggedIn1' }],
        },
        'LoggedIn',
      ),
    ).toEqual(['stack1', 'LoggedIn1']);
  });

  it('should return the keys for a screen in a nested state', () => {
    expect(
      keysForState(
        {
          stale: false,
          type: 'stack',
          key: 'stack1',
          index: 0,
          routeNames: ['LoggedIn', 'LoggedOut'],
          routes: [
            {
              key: 'LoggedIn1',
              name: 'LoggedIn',
              state: {
                stale: false,
                type: 'stack',
                key: 'stack2',
                index: 0,
                routeNames: ['Settings', 'Catalog'],
                routes: [{ name: 'Catalog', key: 'Homepage1' }],
              },
            },
          ],
        },
        'Catalog',
      ),
    ).toEqual(['stack2', 'Homepage1']);
  });

  it('should return the keys for a screen in a deeply nested state', () => {
    expect(
      keysForState(
        {
          stale: false,
          type: 'stack',
          key: 'stack1',
          index: 0,
          routeNames: ['LoggedIn', 'LoggedOut'],
          routes: [
            {
              key: 'LoggedIn1',
              name: 'LoggedIn',
              state: {
                stale: false,
                type: 'stack',
                key: 'stack2',
                index: 1,
                routeNames: ['Payment', 'Catalog'],
                routes: [
                  { name: 'Catalog', key: 'Homepage1' },
                  {
                    key: 'Payment1',
                    name: 'Payment',
                    state: {
                      stale: false,
                      type: 'stack',
                      key: 'stack3',
                      index: 0,
                      routeNames: ['Confirmation', 'Account'],
                      routes: [{ name: 'Account', key: 'Account1' }],
                    },
                  },
                ],
              },
            },
          ],
        },
        'Account',
      ),
    ).toEqual(['stack3', 'Account1']);
  });
});

describe('stringArraysEqual()', () => {
  it('should return true for empty arrays', () => {
    expect(stringArraysEqual([], [])).toBe(true);
  });

  it('should return true for arrays with the same length and same values', () => {
    expect(stringArraysEqual(['1'], ['1'])).toBe(true);
    expect(stringArraysEqual(['1', '2'], ['1', '2'])).toBe(true);
    expect(stringArraysEqual(['1', '2', '3', '4'], ['1', '2', '3', '4'])).toBe(
      true,
    );
  });

  it('should return false for arrays with the same length but different values', () => {
    expect(stringArraysEqual(['1'], ['a'])).toBe(false);
    expect(stringArraysEqual(['1', '2'], ['a', 'b'])).toBe(false);
    expect(stringArraysEqual(['1', '2', '3', '4'], ['a', 'b', 'c', 'd'])).toBe(
      false,
    );
  });

  it('should return false for arrays with the same length and same values in a different order', () => {
    expect(stringArraysEqual(['1', '2'], ['2', '1'])).toBe(false);
    expect(stringArraysEqual(['1', '2', '3', '4'], ['4', '2', '1', '3'])).toBe(
      false,
    );
  });

  it('should return false for arrays with different lengths', () => {
    expect(stringArraysEqual(['1'], [])).toBe(false);
    expect(stringArraysEqual([], ['1'])).toBe(false);
    expect(stringArraysEqual(['1', '2'], ['1'])).toBe(false);
    expect(stringArraysEqual(['1'], ['1', '2'])).toBe(false);
    expect(stringArraysEqual(['1', '2', '3'], ['1', '2'])).toBe(false);
    expect(stringArraysEqual(['1', '2', '3'], ['1'])).toBe(false);
    expect(stringArraysEqual(['1', '2', '3'], [])).toBe(false);
  });
});

const setCurrentRouteName = (name: string) =>
  (navigationRef.current?.getCurrentRoute as jest.Mock).mockReturnValue({
    name,
  });

const setNavigationState = (state: Partial<NavigationState>) =>
  (navigationRef.current?.getRootState as jest.Mock).mockReturnValue(state);

describe('useNavigationStack()', () => {
  let dispatch: jest.Mock;

  beforeEach(() => {
    (navigationRef.current as any) = {
      dispatch: jest.fn(),
      getCurrentRoute: jest.fn(() => ({})),
      getRootState: jest.fn(() => ({})),
    };
    navigationReady.current = true;
    dispatch = navigationRef.current?.dispatch as jest.Mock;
  });

  afterEach(() => {
    navigationReady.current = false;
    (navigationRef.current as any) = undefined;
  });

  it('should throw an error if there is no screen in the stack', () => {
    const { result } = renderHook(() => useNavigationStack(() => []));
    expect(result.error).toEqual(
      expect.objectContaining({
        message: 'useNavigationStack has no screens to render',
      }),
    );
  });

  it('should do nothing if not ready', () => {
    navigationReady.current = false;
    renderHook((props) => useNavigationStack(() => props), {
      initialProps: ['screen1'],
    });
    expect(dispatch).not.toHaveBeenCalled();
  });

  it('should push a single screen to a stack', () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen1'] },
    );
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'PUSH', payload: { name: 'screen1' } }),
    );
    setCurrentRouteName('screen1');
    rerender(['screen2', 'screen1']);
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'PUSH', payload: { name: 'screen2' } }),
    );
    setCurrentRouteName('screen2');
    rerender(['newscreen', 'screen2', 'screen1']);
    expect(dispatch).toHaveBeenCalledTimes(3);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'PUSH', payload: { name: 'newscreen' } }),
    );
  });

  it("shouldn't push the first screen if it's the default initial screen in the stack", () => {
    setCurrentRouteName('first-screen');
    renderHook((props) => useNavigationStack(() => props), {
      initialProps: ['first-screen'],
    });
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  it('should pop a single screen', () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen1'] },
    );
    setCurrentRouteName('screen1');
    rerender(['screen2', 'screen1']);
    dispatch.mockClear();
    setNavigationState({
      index: 1,
      key: 'stack-key',
      routes: [
        { name: 'screen2', key: 'screen2-key' },
        { name: 'screen1', key: 'screen1-key' },
      ],
    });
    setCurrentRouteName('screen2');
    rerender(['screen1']);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'GO_BACK',
        target: 'stack-key',
        source: 'screen2-key',
      }),
    );
  });

  it("shouldn't pop when the back navigation was triggered by the user", () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      {
        initialProps: ['screen2'],
      },
    );
    setCurrentRouteName('screen2');
    rerender(['screen1', 'screen2']);
    dispatch.mockClear();
    setCurrentRouteName('screen1');
    setCurrentRouteName('screen2'); // user swipes back or presses back button
    setNavigationState({
      index: 0,
      key: 'stack-key',
      routes: [{ name: 'screen2', key: 'screen2-key' }],
    });
    rerender(['screen2']);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  it('should replace a single screen', () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen1'] },
    );
    dispatch.mockClear();
    setCurrentRouteName('screen1');
    rerender(['screen2']);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'REPLACE',
        payload: { name: 'screen2' },
      }),
    );
  });

  it('should replace the first screen in a stack', () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen1'] },
    );
    setCurrentRouteName('screen1');
    rerender(['screen2', 'screen1']);
    setCurrentRouteName('screen2');
    rerender(['screen3', 'screen2', 'screen1']);
    setCurrentRouteName('screen3');
    dispatch.mockClear();
    rerender(['newscreen', 'screen2', 'screen1']);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'REPLACE',
        payload: { name: 'newscreen' },
      }),
    );
  });

  it("shouldn't replace the screen if it was already triggered", () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen1'] },
    );
    dispatch.mockClear();
    setCurrentRouteName('screen1');
    setCurrentRouteName('screen2');
    rerender(['screen2']);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  it('should reset the whole stack', () => {
    const { rerender } = renderHook(
      (props) => useNavigationStack(() => props),
      { initialProps: ['screen2'] },
    );
    setCurrentRouteName('screen2');
    rerender(['screen1', 'screen2']);
    dispatch.mockClear();
    setNavigationState({
      index: 1,
      key: 'stack-key',
      routes: [
        { name: 'screen2', key: 'screen2-key' },
        { name: 'screen1', key: 'screen1-key' },
      ],
    });
    setCurrentRouteName('screen1');
    rerender(['newscreen']);
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'POP_TO_TOP' }),
    );
    expect(dispatch).toHaveBeenCalledWith(
      expect.objectContaining({
        type: 'REPLACE',
        payload: { name: 'newscreen' },
      }),
    );
  });
});
