import { formatPrice } from './formatPrice';

describe('formatPrice', () => {
  it('should be to 2 decimal places', () => {
    expect(formatPrice(0)).toBe('0.00');
    expect(formatPrice(10.99)).toBe('10.99');
    expect(formatPrice(101.991)).toBe('101.99');
  });
});
