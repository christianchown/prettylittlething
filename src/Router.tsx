import React, { useMemo } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { EventMapCore, NavigationState } from '@react-navigation/core';
import { matchesState } from 'xstate';
import { useSelector } from '@xstate/react';
import Homepage from 'screens/Homepage';
import Catalog from 'screens/Catalog';
import Basket from 'screens/Basket';
import Loading from 'screens/Loading';
import Payment from 'screens/Payment';
import Error from 'screens/Error';
import ProductPage from 'screens/ProductPage';
import { useStatechart } from 'state/Statechart';
import { historySelector, stateValueSelector } from 'state/selectors';
import { useNavigationStack } from 'lib/useNavigationStack';

const Stack = createNativeStackNavigator();

export default function Router() {
  const service = useStatechart();
  const state = useSelector(service, stateValueSelector);
  const history = useSelector(service, historySelector);
  useNavigationStack(() => {
    if (matchesState(state, 'loading')) {
      return ['Loading'];
    }
    if (matchesState(state, 'error')) {
      return ['Error'];
    }
    if (matchesState(state, 'catalog.homepage')) {
      return ['Homepage'];
    }
    if (matchesState(state, 'catalog.products')) {
      return ['Catalog', 'Homepage'];
    }
    if (matchesState(state, 'catalog.product')) {
      return ['ProductPage', 'Catalog', 'Homepage'];
    }
    const catalog =
      history?.states && history.states?.catalog?.current === 'product'
        ? ['ProductPage', 'Catalog', 'Homepage']
        : history?.states && history.states?.catalog?.current === 'products'
        ? ['Catalog', 'Homepage']
        : ['Homepage'];
    if (matchesState(state, 'checkout.basket')) {
      return ['Basket', ...catalog];
    }
    if (matchesState(state, 'checkout.viewProduct')) {
      return ['ProductPage', 'Basket', ...catalog];
    }
    if (matchesState(state, 'checkout.payment')) {
      return ['Payment', 'Basket', ...catalog];
    }
    return [];
  });
  const listeners = useMemo(
    () => ({
      beforeRemove: (e: unknown) => {
        if (
          (e as EventMapCore<NavigationState<any>>['beforeRemove'])?.data
            ?.action?.type === 'POP'
        ) {
          // dismiss by back link, iOS swipe or Android back button
          service.send('BACK');
        }
      },
    }),
    [service],
  );
  return (
    <Stack.Navigator>
      <Stack.Screen name="Loading" component={Loading} />
      <Stack.Screen
        name="Homepage"
        component={Homepage}
        options={{ title: 'Select category' }}
      />
      <Stack.Screen
        name="Catalog"
        component={Catalog}
        listeners={listeners}
        options={{ title: 'Product catalog' }}
      />
      <Stack.Screen
        name="Basket"
        component={Basket}
        listeners={listeners}
        options={{ title: 'Your bag' }}
      />
      <Stack.Screen name="Payment" component={Payment} listeners={listeners} />
      <Stack.Screen name="Error" component={Error} />
      <Stack.Screen
        name="ProductPage"
        component={ProductPage}
        listeners={listeners}
      />
    </Stack.Navigator>
  );
}
